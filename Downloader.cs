using System.Net;
class Downloader {
    public string[] getRelevantFiles(){
        // Logic to get links to relevant files
        // ...

        // End with an array of strings
        string[] FileURLS = {
            "https://scholar.harvard.edu/files/torman_personal/files/samplepptx.pptx",
            "https://www.adobe.com/support/products/enterprise/knowledgecenter/media/c4611_sample_explain.pdf",
            "https://www.ets.org/s/achievement_gap/rsc/pptx/healthy_school_environments_ngandu.pptx"
        };
        return FileURLS;
    }

    // Start parsing after download.
    public void DownloadFiles(){
        string[] urls = getRelevantFiles();
        Parser parse = new Parser();
        AnalyzeText a_text = new AnalyzeText();
        foreach(string url in urls){
            using(WebClient client = new WebClient())
            {
                if(url.ToLower().Contains(".pdf")) 
                {
                    client.DownloadFile(url, @".\\temp.pdf");
                    a_text.Analyze(parse.pdf_to_text());
                } 
                else if(url.ToLower().Contains(".pptx")) 
                { 
                    client.DownloadFile(url, @".\\temp.pptx");
                    a_text.Analyze(parse.pptx_to_text());
                }
            } 
        }
    }
}