using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using ShapeCrawler;
using System.Text.Json;
using System.Text.Json.Serialization;

  

class Parser {
        public class PPTSlide
    {
        public int SlideNumber { get; set; }
        public string? TitleString { get; set; }
        public string?[] OtherText { get; set; }
    }
    public string pdf_to_text()
    {
        string path = ".\\temp.pdf";
        using (PdfReader reader = new PdfReader(path))
        {
            StringBuilder text = new StringBuilder();
            for(int i = 1; i <= reader.NumberOfPages; i++)
            {
                try {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                } catch (Exception ex) { }
            }
            return text.ToString();
        }
    }
    


    public string pptx_to_text() {
        string final_json = "";

        // Open presentation 
        using IPresentation presentation = SCPresentation.Open(".\\temp.pptx", isEditable: true);
        List<PPTSlide> slideList = new List<PPTSlide>();
        int currentPage;
        for(currentPage = 0; currentPage < presentation.Slides.Count; currentPage++)
        {
        
            var slide = presentation.Slides[currentPage];
            string titleText = "";
            string[] otherText = new string[slide.Shapes.Count];
            
            for(int currentShape = 1; currentShape <= slide.Shapes.Count; currentShape++)
            {      
                // Set title text
                if(currentShape == 1) {
                    try {
                        IAutoShape autoShape = (IAutoShape)slide.Shapes[currentShape - 1];
                        titleText = autoShape.TextBox.Text;
                    } catch (Exception ex ) {
                        var autoShape = slide.Shapes[currentShape - 1];
                        titleText = autoShape.ToString();
                    }
                } else {
                     try {
                        IAutoShape autoShape = (IAutoShape)slide.Shapes[currentShape - 1];
                        otherText[currentShape - 1] = autoShape.TextBox.Text;
                    } catch (Exception ex ) {
                        try {
                            IGroupShape autoShape = (IGroupShape)slide.Shapes[currentShape - 1];
                            otherText[currentShape - 1] = autoShape.ToString();
                        } catch(Exception exs){
                            var autoShape = slide.Shapes[currentShape - 1];
                            otherText[currentShape - 1] = autoShape.ToString();
                        }
                    }
                }
                if( otherText[currentShape - 1] == "ShapeCrawler.SlideAutoShape" || otherText[currentShape - 1] == "ShapeCrawler.SlidePicture"
                    || otherText[currentShape - 1] == "ShapeCrawler.SlideGroupShape") {
                        otherText[currentShape - 1] = "NULL";
                }
                if(titleText == "ShapeCrawler.SlideAutoShape" || titleText == "ShapeCrawler.SlideGroupShape") {
                    titleText = "NULL";
                }
            }

            PPTSlide p_slide = new PPTSlide
            {
                SlideNumber = currentPage,
                TitleString = titleText,
                OtherText = otherText          
            };
            slideList.Add(p_slide);
        }
        final_json = JsonSerializer.Serialize(slideList);
        return final_json;
    }
}
